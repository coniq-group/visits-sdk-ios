```
At first, here is description of used interfaces and field.
Below you will find an instruction with 7 steps on how to setup SDK.

import Foundation
import UIKit
import CoreLocation
import ConiqGeofencingSDK


// CNQ_GeofenceSDK - is the type of SDK interface. Below is the full list of available methods

public class CNQ_GeofenceSDK {

    /*
     initializes SDK, setting initial data such as:
        cnq_client_id,
        cnq_client_secret,
        cnq_platform_url,
        cnq_api_url
    */
     public init(data: ConiqGeofencingSDK.InitialData)

     /*
      setting the delegate class object, which will invoke callback, once the SDK detects the user crossing geofence border. The class object must conform to CNQ_GeofenceBreakDelegate protocol
     */
     public func setGeofenceBreakDelegate(delegateObj: ConiqGeofencingSDK.CNQ_GeofenceBreakDelegate)

     /*
      Allow to enable/disable notifications shown to user on geofence border crossing
     */
     public func enableNotifications(_ enable: Bool)

     /*
      Useful for debugging. Returns log of events happened within SDK - api calls, location changes etc
     */
     public func getLogHistory() -> String

     /*
      Return ID of geofence which user is currently in, or empty if none
     */
     public func getCurrentUserGeofence() -> String

     /*
      Setting current list of geofences to be tracked. Previous list will be cleared
     */
     public func setGeofences(_ geoNotifications: [NSDictionary])

     /*
      Removing certain geofences from list and stop tracking for them
     */
     public func removeGeofences(_ ids: [String])

     /*
      Clearing the entire list of geofences and stop tracking for all of them
     */
     public func removeAllGeofences()
}




Below is the interface and description of each field of Geofence object that is passed to SDK:

let exampleGeofence: NSDictionary = [
"id" : id, // unique id of geofence
"latitude" : Float, // latitude coordinates of center point of geofence cirlce
"longitude" : Float(lng), // longitude coordinates of center point of geofence cirlce
"radius" : Float(radius), // radius of geofence cirlce
"notificationEnter" : [ // notification object for when user entering geofence
"id" : 2133977355, // unique ID of notification object
"title" : "Welcome to our great SHOPPING MALL!", // notification header that will be shown to user
"frequency" : "daily", // frequency of showing notification. Possible options: 'daily','weekly','monthly','unlimited','once'. Notification will be shown not more then set by frequency. For example, if set to "weekly", user will not get notification shown earlier than week after previous. This is related ONLY to notifications, and not influencing API calls to platform
"text" : "We wish you an amazing shopping!" // notification body
],
"notificationExit" : [
"id" : 2133977356,
"title" : "Thank you for spending you salary here! ",
"frequency" : "daily",
"text" : "Come back tomorrow with a credit from the bank allowed :D "
],
"transitionType" : 3, // there are three types exists 1 - enter only, 2 - exit only, 3 - both. Setting transition type influence both - API calls and notifications. If notification type will be set to 1 for example, exits of geofence will be ignored entirely
]



This example class below will show how to use ConiqGeofencingSDK.
Follow 7 steps to setup SDK and required variables.
*/


class ExampleUsageClass: NSObject, CNQ_GeofenceBreakDelegate {
/*
==========>  1) At first, you need to create variable of SDK interface type
*/
var geoSdk: CNQ_GeofenceSDK

    /* access and refresh tokens are declared here just for convenience, they will contain tokens that your application will use for authentication on Coniq platform
     
            also shared access&refresh tokens: UserDefaults.standard.value(forKey: "cnq_access_token") can be used instead. In that case you'll store and use tokens from UserDefaults
     */
    var accessTokenForAPICalls = ""
    var refreshTokenForAPICalls = ""
    
    override init() {
        
        /*
==========> 2) Ask for notification permissions, as they must be granted before SDK initialization
*/
UNUserNotificationCenter.current().requestAuthorization(options: [UNAuthorizationOptions.badge, UNAuthorizationOptions.alert, UNAuthorizationOptions.sound], completionHandler: {res, x in NSLog("notification granted")})



        /*
==========> 3) Initialize SDK object with data, that should contain client_id, client_secret (you should receive them after platform account creation). Also you can change platform url, and api url if neeeded (probably not)
*/
let dataForSdk = InitialData(
cnq_client_id: "1f12f21f121f",
cnq_client_secret: "f12f1212f1",
cnq_platform_url: "https://platform.coniq.com",
cnq_api_url: "https://api.coniq.com")

        geoSdk = CNQ_GeofenceSDK(data: dataForSdk)
        super.init()
       

        /*
         The code below is just an example of a possible way of obtaining user access and refresh tokens
         */
        //  let response = HTTP.post('https://coniq.xom/coniqAPI/login', {"john-galt@mail.com", "Password123"})
        let fakeResponse: NSDictionary = ["access_token": "aff831f0aafaf3f32f2cc22c3", "refresh_token": "aff831f0aafaf3f32f2cc22c3"]
        
        
        self.accessTokenForAPICalls = fakeResponse.value(forKey: "access_token") as! String
        self.refreshTokenForAPICalls = fakeResponse.value(forKey: "refresh_token") as! String
        
        /*
==========> 4) After user performs login in your app, you'll need to write access and refresh tokens in UserDefaults using keys `cnq_access_token` and `cnq_refresh_token`. That values will be used to send requests to Coniq platoform once user crosses geofence border
*/
// IMPORTANT: Once user has been authenticated on platform, his access & refresh tokens must be written in NSUserDefaults
UserDefaults.standard.setValue(self.accessTokenForAPICalls, forKey: "cnq_access_token")
UserDefaults.standard.setValue(self.refreshTokenForAPICalls, forKey: "cnq_refresh_token")

        /*
==========> 5a) You should subscribe to updating that keys, because your app can be launched in background, if user cross the geofence border, and SDK will refresh outdated access token.
*/
UserDefaults.standard.addObserver(self, forKeyPath: "cnq_access_token", options: .new, context: nil)
UserDefaults.standard.addObserver(self, forKeyPath: "cnq_refresh_token", options: .new, context: nil)

        /*
==========> 6) (optional) If you want to have callback and get notified when user crossing geofence border, set delegate class object, that implementing CNQ_GeofenceBreakDelegate protocol
- you have to conform your class CNQ_GeofenceBreakDelegate and implement method
*/
self.geoSdk.setGeofenceBreakDelegate(delegateObj: self)

    }
    
    /*
==========> 5b) You must always keep your access and refresh tokens synchronized with tokens, stored in UserDefaults!
*/
// IMPORTANT: developers must subscribe for tokens change
override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
if keyPath == "cnq_access_token" {
self.accessTokenForAPICalls = UserDefaults.standard.value(forKey: "cnq_access_token") as! String
}

        if keyPath == "cnq_refresh_token" {
            self.refreshTokenForAPICalls =  UserDefaults.standard.value(forKey: "cnq_refresh_token") as! String
        }
        
        UserDefaults.standard.synchronize()
    }
    
    
    
    func addGeofences(id: String, lat: String, lng: String, radius: String) {
      
        let geofence1: NSDictionary = [
            "latitude" : Float(lat),
            "longitude" : Float(lng),
            "radius" : Float(radius),
          "notificationEnter" : [
            "id" : 2133977355,
            "title" : "Welcome to our great SHOPPING MALL! :) ",
            "frequency" : "daily",
            "openAppOnClick" : true,
            "text" : "We wish you an amazing shopping! (Spend all your salary :P - you won't regret it!) "
          ],
          "id" : id,
          "notificationExit" : [
            "id" : 2133977356,
            "title" : "Thank you for spending you salary here! ",
            "frequency" : "daily",
            "text" : "Come back tomorrow with a credit from the bank allowed :D "
          ],
          "transitionType" : 3,
          
          
        ]
        /*
    ==========> 7) Add up to 20 geofences to start tracking user being crossed them. Once you called "setGeofences", all previous geofences will be cleared
         */
        geoSdk.setGeofences([geofence1])
    }
    
    
    /*
     This is callback, that get called when user crossing border of geofence - in or out, depending on settings
     */
    func onGeofenceBreak(geofenceId: String, type: GeofenceCrossType) {
        NSLog("Geofence has been crossed!::" + geofenceId)
    }
}
```
---
### Limitations
- Max 20 geofences can be monitored at a time  
In order to monitor more than 20 geofences, you'll need to call `setGeofences` to update the list of geofences as the user moves so that the nearest 20 geofences are always monitored.
- The minimum radius of a geofence is 100m  
But, it's recommended to be larger (at least 200m) because the smaller the radius is, the less are the chances of a geofence break to be detected.
- Requires location permission to be granted even when the app is in background or not in use (i.e. _"Always"_). It needs to be granted before the geofence monitoring starts.
