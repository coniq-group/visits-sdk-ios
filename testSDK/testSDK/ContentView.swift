//
//  ContentView.swift
//  testSDK
//
//  Created by User on 31.10.22.
//

import SwiftUI
import CoreLocation


struct ContentView: View {
    var q: ExampleUsageClass

    @State private var geofence_id: String = "53f44907-af17-4a78-b6ec-94f2cf79c21c"
    @State private var lat: String = "51.519776"
    @State private var lng: String = "-0.098998"
    @State private var radius: String = "1000"
    
    @State private var accessTokenForAPICalls: String = ""
    @State private var refreshTokenForAPICalls: String = ""
    @State private var cnq_api_url: String = "https://api.coniq.com"
    @State private var cnq_platform_url: String = "https://platform.coniq.com"
    @State private var cnq_client_id: String = ""
    @State private var cnq_client_secret: String = ""
    
    
    var body: some View {
       
        
        Text("1. Please press buttons and complete actions step by step! After completing step #3 in the bottom of screen you can move app to background (or kill it) and trigger location change")
            .padding()
        Button(
          "a. Request when in use",
          action: {
              CLLocationManager().requestWhenInUseAuthorization()
          }
        )
        
        Button(
          "b. Request Always",
          action: {
              CLLocationManager().requestAlwaysAuthorization()
          }
        )
        
        Text("2. Fill the Enter coordinates of geofence. LAT, LNG, RADUIS. Fill access/refresh tokens, client_id, client_secret")
            .padding()
        

        Form {
            TextField(text: $geofence_id, prompt: Text("Geofence ID from platform")) {}
            TextField(text: $lat, prompt: Text("Geofence center LAT")) {}
            TextField(text: $lng, prompt: Text("Geofence center LNG")) {}
            TextField(text: $radius, prompt: Text("Geofence radius")) {
                
            }
            
            TextField(text: $accessTokenForAPICalls, prompt: Text("accessTokenForAPICalls")) {}
            TextField(text: $refreshTokenForAPICalls, prompt: Text("refreshTokenForAPICalls")) {}
            
            TextField(text: $cnq_client_id, prompt: Text("platform_client_id")) {}
            TextField(text: $cnq_client_secret, prompt: Text("platform_client_secret")) {}
            
            TextField(text: $cnq_api_url, prompt: Text("API url")) {}
            TextField(text: $cnq_platform_url, prompt: Text("platform url")) {}
        }
//        Text("LNG:")
//            TextField(text: $lng, prompt: Text("Longitude in format xx.xxxxx")) {
//                Text("Geofence LNG")
//            }
//        Text("RADIUS:")
//            TextField(text: $radius, prompt: Text("Radius in meters")) {
//                Text("Geofence Radius")
//            }
            
       // }
        

        
        
        Button(
          "3. Apply data from form",
          action: {
              q.addGeofences(id: geofence_id, lat: lat, lng: lng, radius: radius)
              
              UserDefaults.standard.setValue(accessTokenForAPICalls, forKey: "cnq_access_token")
              UserDefaults.standard.setValue(refreshTokenForAPICalls, forKey: "cnq_refresh_token")
              
              UserDefaults.standard.setValue(cnq_api_url, forKey: "cnq_api_url")
              UserDefaults.standard.setValue(cnq_platform_url, forKey: "cnq_platform_url")
              UserDefaults.standard.setValue(cnq_client_id, forKey: "cnq_client_id")
              UserDefaults.standard.setValue(cnq_client_secret, forKey: "cnq_client_secret")
          }
        )
    
        
        
    }
    
    init() {
        self.q = ExampleUsageClass()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
