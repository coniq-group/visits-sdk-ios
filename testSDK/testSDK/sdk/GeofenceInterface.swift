import Foundation
import UIKit
import CoreLocation
import ConiqGeofencingSDK

class ExampleUsageClass: NSObject, CNQ_GeofenceBreakDelegate {
    
    var geoSdk: CNQ_GeofenceSDK
    
    //also shared access&refresh tokens: UserDefaults.standard.value(forKey: "cnq_access_token") can be used instead. In that case you'll store and use tokens from UserDefaults
    var accessTokenForAPICalls = ""
    var refreshTokenForAPICalls = ""
    
    override init() {
        let dataForSdk = InitialData(
            cnq_client_id: "1f12f21f121f",
            cnq_client_secret: "f12f1212f1",
            cnq_platform_url: "https://platform.coniq.com",
            cnq_api_url: "https://api.coniq.com")
        
     
        UNUserNotificationCenter.current().requestAuthorization(options: [UNAuthorizationOptions.badge, UNAuthorizationOptions.alert, UNAuthorizationOptions.sound], completionHandler: {res, x in NSLog("notification granted")})
       
        
        geoSdk = CNQ_GeofenceSDK(data: dataForSdk)
        super.init()
       
        
        //  let response = HTTP.post('https://coniq.xom/coniqAPI/login', {"john-galt@mail.com", "Password123"})
        let fakeResponse: NSDictionary = ["access_token": "aff831f0aafaf3f32f2cc22c3", "refresh_token": "aff831f0aafaf3f32f2cc22c3"]
        
        
        self.accessTokenForAPICalls = fakeResponse.value(forKey: "access_token") as! String
        self.refreshTokenForAPICalls = fakeResponse.value(forKey: "refresh_token") as! String
        
        
        // IMPORTANT: Once user has been authenticated on platform, his access & refresh tokens must be written in NSUserDefaults
       // UserDefaults.standard.setValue(self.accessTokenForAPICalls, forKey: "cnq_access_token")
       // UserDefaults.standard.setValue(self.refreshTokenForAPICalls, forKey: "cnq_refresh_token")
        
        UserDefaults.standard.addObserver(self, forKeyPath: "cnq_access_token", options: .new, context: nil)
        UserDefaults.standard.addObserver(self, forKeyPath: "cnq_refresh_token", options: .new, context: nil)
        
        self.geoSdk.setGeofenceBreakDelegate(delegateObj: self)
    
    }
    
    
    // IMPORTANT: developers must subscribe for tokens change
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "cnq_access_token" {
            self.accessTokenForAPICalls = UserDefaults.standard.value(forKey: "cnq_access_token") as! String
        }
        
        if keyPath == "cnq_refresh_token" {
            self.refreshTokenForAPICalls =  UserDefaults.standard.value(forKey: "cnq_refresh_token") as! String
        }
        
        UserDefaults.standard.synchronize()
    }
    
    
    
    func addGeofences(id: String, lat: String, lng: String, radius: String) {
      
        let geofence1: NSDictionary = [
            "latitude" : Float(lat),
            "longitude" : Float(lng),
            "radius" : Float(radius),
          "notificationEnter" : [
            "id" : 2133977355,
            "title" : "Welcome to our great SHOPPING MALL! :) ",
            "frequency" : "daily",
            "openAppOnClick" : true,
            "text" : "We wish you an amazing shopping! (Spend all your salary :P - you won't regret it!) "
          ],
          "id" : id,
          "notificationExit" : [
            "id" : 2133977356,
            "title" : "Thank you for spending you salary here! ",
            "frequency" : "daily",
            "openAppOnClick" : true,
            "text" : "Come back tomorrow with a credit from the bank allowed :D "
          ],
          "transitionType" : 3,
          
          
        ]
     
        
      
        
        geoSdk.setGeofences([geofence1])
    }
    
    func onGeofenceBreak(geofenceId: String, type: GeofenceCrossType) {
        NSLog("================Geofence has been crossed!::" + geofenceId)
    }
}







